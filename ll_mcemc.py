#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  6 13:30:42 2021

@author: Rainier
"""

from mpi4py import MPI                                                          


#print('hello')
import numpy as np
import time
import loading_dragonfly_data as load
import mcemc      
import sys
import os
from scipy.spatial.transform import Rotation as R
import matplotlib.pyplot as plt


def hi( string = None, N = None ):
    comm = MPI.COMM_WORLD
    p = comm.rank
    string_ = "hi i'm process "
    string_ += str(p)
    if string != None:
        string_ += " i'm doing "
        string_ += string + ' '
    if N != None:
        string_ += str(N)
    print( string_ )
    sys.stdout.flush()


def make_save_dir(args):
    # unpacking args
    save_models = args['save_models']
    save_plots = args['save_plots']
    
    comm = MPI.COMM_WORLD
    n_rank = comm.rank 
    np.random.seed( n_rank )
    if n_rank == 0:
        # making our directory to write into
        save_dir = mcemc.make_date_dir()
        save_dir_out = save_dir+'out_files/' 
        os.mkdir( save_dir_out )
        
        if save_plots:# making plots dircetory
            save_dir_plots = save_dir+'plots/'
            os.mkdir( save_dir_plots )
            os.mkdir( save_dir_plots+'model_plots/' )
                
        if save_models:# making model directory
            save_dir_models = save_dir+'models/'
            os.mkdir( save_dir_models )
    else:
        save_dir = None
    save_dir = comm.bcast(save_dir, root=0)
    return save_dir
   
    
def orientation_match(M_best,M,mask_r=1,N_it=2):
# outputs q_to_best on 0 and None otherwise
    comm = MPI.COMM_WORLD
    p = comm.rank 
    N_p = comm.size
    N_orientation_tiles = load.get_N_rotations()
    N_rp = N_orientation_tiles/N_p
    N_favs = 4
    N_edge = np.shape(M_best)[0]
    n_start = int( p * N_rp )
    n_end = int( (p+1) * N_rp )
   
    cube_gen = [-1,0,1]
    c = 2*((3*np.pi)/(2*N_orientation_tiles))**(1/3)
    q_zooms = np.empty((len(cube_gen)**4,5))
    q_tiles = np.empty((n_end-n_start,5))
    q_tiles[:,0:4] = load.load_orientation_tiles(n_start, n_end)
    q_fav = np.empty(5, dtype=np.float64)
    mask = mcemc.sphere_mask(mask_r,N_edge)
    if p == 0:
            favs_g = np.empty((N_favs*N_p,5), dtype=np.float64)
    else:
       favs_g = None
    
    def calc_and_Gather(qs,favs_g=favs_g):
        for q in qs:
            q[4] = mcemc.calc_error(q[0:4],M_best,M,mask)# 5th assigned as error
        qs=qs[qs[:, 4].argsort()]
        favs_l = qs[0:N_favs]
        comm.Gather(favs_l, favs_g, root=0)
        if p == 0:
            favs_g= favs_g[favs_g[:, 4].argsort()]
            send = favs_g[0:N_p]
        else:
            send = None
        comm.Scatter(send,q_fav,root=0)
        
    def gen_q_zooms(q,c):
        i = 0
        for w in cube_gen:
            for x in cube_gen:
                for y in cube_gen:
                    for z in cube_gen:
                        q_ = q[0:4]+c*np.array([w,x,y,z])
                        q_[0:4] /= np.sqrt(np.sum(q_[0:4]**2))
                        q_zooms[i,0:4] = q_
                        i+=1
                        
    hi(string='calc_and_Gather tiles')                    
    calc_and_Gather(q_tiles)
    for i in range(N_it):
        hi(string='gen_q_zooms',N=i)
        gen_q_zooms(q_fav, c)
        hi(string='calc_and_Gather',N=i)
        calc_and_Gather(q_zooms)
        c/=2
     
    if p == 0:
        return favs_g[0,0:4]
    else:
        return None
    
    comm.Barrier()

    
def plot_results_ll(q_mc_to_best,M_mcemc,M_best,plot_dir,models_dir,mask_r = 1,clim=None):
    comm = MPI.COMM_WORLD
    p = comm.rank
    N_p = comm.size
    
    if 0 % N_p == p:
        print('plotting results panel')
        sys.stdout.flush()
        N_edge = np.shape(M_best)[0]
        mask = mcemc.sphere_mask( mask_r , N_edge)
        mcemc.plot_result_mc(M_mcemc,M_best,q_mc_to_best,
                             mask,models_dir,plot_dir,clim=clim)
        print('plotting results panel done')
    if 1 % N_p == p:   
        print('plotting triple progression')
        sys.stdout.flush()
        triple_progression_name=plot_dir+'triple_progression.pdf'
        mcemc.plot_triple_progression(models_dir,6,save_name=triple_progression_name,mask_r=mask_r,clim=clim)
        print('plotting triple progression done')
    if 2 % N_p == p:   
        print('plotting progression')
        sys.stdout.flush()
        progression_name=plot_dir+'progression.pdf'
        mcemc.plot_progression(save_dir_models=models_dir,save_name=progression_name,mask_r=mask_r,clim=clim)
        print('plotting progression done')
     
        
def calc_J_autocov(J_record,save_dir_plots=None):
    hi(string='calculating J autocov')  
    comm = MPI.COMM_WORLD
    p = comm.rank
    N_p = comm.size
    J_record=np.load(J_record)
    N_iter,N_shots,N_metro=J_record.shape
    N_spc = N_shots / N_p# shots per worker node
    n_shot_start = int( p * N_spc )# This scheme gives core 0 some data
    n_shot_end = int( ( p + 1 ) * N_spc )
    N_shots_local=n_shot_end-n_shot_start
    J_record_local=J_record[:,n_shot_start:n_shot_end,:]
    tau=int(N_metro/2)
    N = N_metro-tau
    ac_avg_loc=np.empty(N,dtype=np.float64)
    ac =np.empty((N_iter,N_shots_local,N),dtype=np.float64)
    for T in range(N):
        #print(T/N)
        #sys.stdout.flush()
        ac[:,:,T]=(
          (J_record_local[:,:,0:tau]*J_record_local[:,:,T:T+tau]).sum(axis=2)/tau 
          -J_record_local[:,:,0:tau].sum(axis=2)*J_record_local[:,:,T:T+tau].sum(axis=2)/tau**2
                  )
    T_a = np.full((N_iter,N_shots_local,N), np.arange(N,dtype=np.float64))
    t0_loc = (T_a * np.abs(ac)).sum()/np.abs(ac).sum()
    t0_loc_a = np.array( [t0_loc], dtype=np.float64)
    ac_avg_loc=(ac**2).sum(axis=(0,1)) 
    if p==0:
        ac_avg=np.empty(N,dtype=np.float64)
        t0_sum =  np.empty(1,dtype=np.float64)
    else:
        ac_avg=None
        t0_sum = None
    comm.Reduce( ac_avg_loc, ac_avg, op = MPI.SUM, root = 0 ) 
    comm.Reduce(t0_loc_a, t0_sum, op = MPI.SUM, root = 0 )
    if p==0:
        t0 = t0_sum[0]/N_p
        hi(string='calc t0_J as', N=t0)
        ac_avg=np.sqrt(ac_avg)
        if save_dir_plots is None:
            plt.plot(ac_avg)
            plt.show()
        else:
            plt.plot(ac_avg)
            plt.title(str(t0))
            plt.savefig(save_dir_plots+'J_autocov.pdf') 
            plt.clf()
            
            
def calc_angle_autocov(angle_record_file,save_dir_plots=None):
    hi(string='calculating rotvec autocov')  
    comm = MPI.COMM_WORLD
    p = comm.rank
    N_p = comm.size
    angle_record=np.load(angle_record_file)
    
    N_iter, N_shots, N_metro, l = angle_record.shape
    
    N_spc = N_shots / N_p# shots per worker node
    n_shot_start = int( p * N_spc )# This scheme gives core 0 some data
    n_shot_end = int( ( p + 1 ) * N_spc )
    N_shots_local=n_shot_end-n_shot_start
    angle_record_local=angle_record[:,n_shot_start:n_shot_end,:,:]
    tau=int(N_metro/2)
    N = N_metro-tau
    ac_avg_loc=np.empty(N,dtype=np.float64)
    ac=np.empty((N_iter,N_shots_local,N,l),dtype=np.float64)
    for T in range(N):
        #print(T/N)
        #sys.stdout.flush()
        ac[:,:,T,:]=(
          (angle_record_local[:,:,0:tau,:]*angle_record_local[:,:,T:T+tau,:]).sum(axis=2)/tau 
          -angle_record_local[:,:,0:tau,:].sum(axis=2)*angle_record_local[:,:,T:T+tau,:].sum(axis=2)/tau**2
                  )
    T_a = np.full((N_iter,N_shots_local,N), np.arange(N,dtype=np.float64))
    t0_loc= (T_a * np.abs(ac.sum(axis=3))).sum()/np.abs(ac).sum()
    t0_loc_a = np.array([t0_loc], dtype=np.float64)
    ac_avg_loc=(ac.sum(axis=3)**2).sum(axis=(0,1))
    if p==0:
        ac_avg=np.empty(N,dtype=np.float64)
        t0_avg=np.empty(1,dtype=np.float64)
    else:
        ac_avg=None
        t0_avg = None
    comm.Reduce( ac_avg_loc, ac_avg, op = MPI.SUM, root = 0 ) 
    comm.Reduce(t0_loc_a, t0_avg, op = MPI.SUM, root = 0 )
    if p==0:
        t0 = t0_avg[0]/N_p
        hi(string='calc t0_angle as', N=t0)
        ac_avg=np.sqrt(ac_avg)
        if save_dir_plots is None:
            plt.plot(ac_avg)
            plt.show()
        else:
            plt.plot(ac_avg)
            plt.title(str(t0))
            plt.savefig(save_dir_plots+'angle_autocov.pdf') 
            plt.clf()
        
        
def J_proj(M,data_dict,comm,n_rank,N_shots):
    if n_rank==0:
        hi(string='J_proj 0 buffers')
        data_ewald_sum_send = np.zeros(1,dtype=np.float64)
        data_ewald_sum_recv = np.zeros(1,dtype=np.float64)
        hi(string='0 J_proj buffers done')
    else:
        hi(string='J_proj data sum')
        data_ewald_sum = mcemc.data_ewald_sum(data_dict)
        hi(string='J_proj else send')
        data_ewald_sum_send = np.array([data_ewald_sum],dtype=np.float64)
        data_ewald_sum_recv = None
        hi(string='J_proj else done')
    comm.Reduce( data_ewald_sum_send, data_ewald_sum_recv, op=MPI.SUM, root=0)
    if n_rank == 0:
        hi(string='J_proj 0 mean')
        model_ewald_mean = mcemc.model_ewald_mean(M)
        data_ewald_mean=data_ewald_sum_recv[0]/(N_shots)
        hi(string='J_proj 0 scaling')
        c = data_ewald_mean/model_ewald_mean
        M*=c
        
        
def merge_given_Rs_Js_ll( args, save_dir='', rot90numz = 1 , zto = 'y', inv = False, redirect_std=False ):

    # unpacking args
    N_shots = args['N_shots']
    N_edge = args['N_edge']
    save_plots = args['save_plots']
    save_models = args['save_models']
    var_J = args['variable_J']
    check_mem=False
    
    comm = MPI.COMM_WORLD
    n_rank = comm.rank 
    N_cores = comm.size
    np.random.seed( n_rank )
        
    if redirect_std and save_dir != '':
    # Setting up stdout and save directories
        save_dir_out = save_dir+'out_files/' 
        outfile = save_dir_out + 'out' + mcemc.number_string(N_cores,n_rank) + '.txt'
        errfile = save_dir_out + 'err' + mcemc.number_string(N_cores,n_rank) + '.txt'
        sys.stdout = open(outfile, 'w')
        sys.stderr = open(errfile, 'w')
        hi()

    N_spc = N_shots / N_cores# shots per worker node
    n_shot_start = int( n_rank * N_spc )# This scheme gives core 0 some data
    n_shot_end = int( ( n_rank + 1 ) * N_spc )
    
    hi( string = 'load frames '+str(n_shot_start)+' to '+str(n_shot_end ) )
    data_dict = load.load_frames( n_shot_start, n_shot_end, check_mem=check_mem )
      
    # loading the rotations as scipy
    Rs = load.load_rotations(n_shot_start, n_shot_end,
                             rot90numz = rot90numz, zto = zto, inv = inv)
    if var_J:
        Js = load.load_Js(n_shot_start, n_shot_end)
    else:
        Js = np.array([ load.get_J0() for i in range( n_shot_start, n_shot_end ) ])
    
    arr2 = mcemc.reconstruction_given_Rs_Js_ll( Rs, Js, data_dict, args )
    
    if n_rank ==0:
        arr2_sum = np.empty( [ N_edge, N_edge, N_edge, 2 ] )
    else:
        arr2_sum = None
    hi(string = 'reducing arr2_g')
    comm.Reduce( arr2, arr2_sum, op = MPI.SUM, root = 0 )
    
    if n_rank == 0:
    # Having the master node turn arr2_g_sum into M_int
        hi( string = ' model given Rs and Js' )
        M = mcemc.M_from_arr2(arr2_sum,fill_zeros=False)
        if save_plots:
            name='M_best.pdf'
            mcemc.plot_triple_view(M, save_name=save_dir+'plots/'+name)
            
        if save_models:
            file_name = ('best_merges_given_params/'
                         +'M_best_given'+str(N_shots)+'_edge'+str(N_edge))
            if not var_J:
                file_name+='const_J'
            if os.path.isfile(file_name):
                os.remove(file_name)
                print('removed '+file_name)
            np.save( file_name, M )
            print('saved ',file_name)
    if redirect_std and save_dir != '':
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__
    comm.Barrier()     

                   
def ll_mcemc( args, save_dir = '', redirect_std = False):
    
    # unpacking args
    N_edge = args['N_edge']
    N_shots = args['N_shots']
    N_iter = args['N_iter']
    N_metro = args['N_metro']
    N_burn = args['N_burn']
    sig_rot_p = args['sig_rot_p']
    sig_J_on_J0 = args['sig_J_on_J0']
    state_seed = args['state_seed']
    save_models = args['save_models']
    save_plots = args['save_plots']
    record_angle = args['record_angle'] 
    record_Js = args['record_Js'] 
    track_logl = args['track_logl']
    variable_J = args['variable_J']
    check_mem = False
    
    comm = MPI.COMM_WORLD
    n_rank = comm.rank 
    N_cores = comm.size
    np.random.seed( n_rank )
        
    def make_info_file(dirr,t):
        deets = (
                     'N_cores = ' + str(N_cores)
            + '\n' + 'N_edge = ' + str(N_edge)
            + '\n' + 'N_shots = ' + str(N_shots)
            + '\n' + 'N_spc = ' + str(np.around(N_spc,2))
            + '\n' + 'N_iter = ' + str(N_iter)
            + '\n' + 'N_metro = ' + str(N_metro)
            + '\n' + 'N_burn = ' + str(N_burn)
            + '\n' + 'sig_rot_p = ' + str(sig_rot_p/np.pi) + ' pi'
            + '\n' + 'sig_J_on_J0 = ' + str(sig_J_on_J0)
            + '\n' + 'variable_J = ' + str(variable_J)
            + '\n' + 'completion time = ' + mcemc.sec_to_hrminsec_str(t)
                 )     
        text_file = open( dirr + 'info' + '.txt', 'w+' )
        text_file.write(deets)
        text_file.close()
    
    if save_dir != '':
    # Setting up stdout and save directories
        if n_rank == 0: 
            if save_plots:# making plots dircetory
                save_dir_plots = save_dir+'plots/'
            if save_models:# making model directory
                save_dir_models = save_dir+'models/'
        if redirect_std:
            hi(string='redirecting std')
            save_dir_out = save_dir+'out_files/'
            outfile = save_dir_out + 'out' + mcemc.number_string(N_cores,n_rank) + '.txt'
            errfile = save_dir_out + 'err' + mcemc.number_string(N_cores,n_rank) + '.txt'
            sys.stdout = open(outfile, 'w')
            sys.stderr = open(errfile, 'w')
    hi()
   
    N_spc = N_shots / ( N_cores - 1 )# shots per worker node
   
    if n_rank == 0:
        ti = time.time() 
        data_dict = None
        
        # Initializing guessed model 
        hi( string = 'initialize arr2_g' )
        arr2_g_sum = np.empty( [ N_edge, N_edge, N_edge, 2 ] )
        arr2_g = np.zeros( [ N_edge, N_edge, N_edge, 2 ] )
            
    else:
        M = np.empty( [ N_edge, N_edge, N_edge ] )# initializing dummy Ms
        # assigning everyone a range of shots to work on
        n_shot_start = int( (n_rank-1) * N_spc )
        n_shot_end = int( n_rank * N_spc )
        N_shots_local = n_shot_end - n_shot_start
        
        #loading frames
        hi( string = 'load frames '+str(n_shot_start)+' to '+str(n_shot_end ) )
        data_dict = load.load_frames( n_shot_start, n_shot_end, check_mem=check_mem )
        
        J0 = load.get_J0()
        
        # Guessed Rs & Js 
        Rs_random = [ mcemc.random_R() for i in range( n_shot_start, n_shot_end ) ]
        if variable_J:
            Js_random = J0*np.abs((1+sig_J_on_J0*np.random.standard_normal(n_shot_end-n_shot_start)))
        else:
            Js_random = np.full(n_shot_end-n_shot_start,J0)
              
        # if we want to start each pattern's walk
        #   in the last position of the previous
        if state_seed:
            Rs_state = Rs_random# initialize these with the guesses
            Js_state = Js_random
            args['Rs_state'] = Rs_state
            args['Js_state'] = Js_state
        '''
        # initial merge the straight forward way with random Rs and Js
        arr2_g = mcemc.reconstruction_given_Rs_Js_ll( Rs_random, Js_random,
                                                      data_dict, args )
        arr2_g_sum = None
    
    # Summing the initial merge on master node
    hi(string = 'reducing arr2_g')
    comm.Reduce( arr2_g, arr2_g_sum, op = MPI.SUM, root = 0 )
    '''
    #Making M0
    if n_rank == 0:
        hi( string = ' initial M' )
        #M = mcemc.M_from_arr2(arr2_g_sum)
        M = np.random.uniform(size=(N_edge,N_edge,N_edge))
    else:
        M = np.empty((N_edge,N_edge,N_edge),dtype=np.float64)
    J_proj(M,data_dict,comm,n_rank,N_shots)
    
    if n_rank == 0:
        
        if save_plots:
            save_name = save_dir_plots+'model_plots/M_'+mcemc.number_string(N_iter, 0)+'.pdf'
            mcemc.plot_triple_view(M, save_name=save_name)
        if save_models:
            np.save(save_dir_models+'M'+mcemc.number_string(N_iter, 0), M)

        if record_angle:# initializing array for angle record
           angle_record = np.empty( (N_iter,N_shots,N_metro,3), dtype=np.float64)
        if record_Js:# initializing array for J record
           J_record = np.empty( (N_iter,N_shots,N_metro), dtype=np.float64)
        if track_logl:
            logls_ave = np.empty((N_iter),dtype=np.float64)
    
    for i in range( N_iter ):
    # The EMC interations
        # broadcast M
        hi( string = 'model broadcast', N = i )
        comm.Bcast( M, root = 0 )
        
        # Checking for inf and nan in the model
        assert(np.isinf(M).any() == False)
        assert(np.isnan(M).any() == False)
        assert((M==0).any() == False)
      
        if n_rank == 0:  
        # Initializing master to receive worker calculations
            arr2_sum = np.zeros( [ N_edge, N_edge, N_edge, 2 ],dtype=np.float64)
            arr2 = np.zeros( [ N_edge, N_edge, N_edge, 2 ],dtype=np.float64 )
            
        else:
            # Initializing workers with business arrays
            arr2_sum = None
            arr2 = np.zeros( [ N_edge, N_edge, N_edge, 2 ],dtype=np.float64 )
            if record_angle:# initializing rect arr to send Gatherv
                angle_record_local = np.empty((N_shots_local,N_metro,3),
                                              dtype=np.float64 )
                args['angle_record_arr'] = angle_record_local
            if record_Js:# initializing rect arr to send Gatherv
                J_record_local = np.zeros((N_shots_local,N_metro),
                                          dtype=np.float64)
                args['J_record_arr'] = J_record_local
            if track_logl:
                logl_subtot = np.empty((1),dtype=np.float64)
                args['logl_subtot'] = logl_subtot
                
            hi( string = 'data loop', N = i)
            mcemc.shot_loop_ll( arr2, M, data_dict, args )
                  
        # Adding into master 
        hi( string = 'reduce', N = i )
        comm.Reduce( arr2, arr2_sum, op = MPI.SUM, root = 0 )
 
        #  Updating M
        if n_rank == 0:   
            hi( string = 'new model', N = i )
            mcemc.update_M_with_arr2(M,arr2_sum)
        J_proj(M,data_dict,comm,n_rank,N_shots)
        
        if n_rank == 0:            
            # Things we want to do each iteration needs to happen here
            if save_plots:
                save_name = save_dir_plots+'model_plots/M_'+mcemc.number_string(N_iter,i+1)+'.pdf'
                mcemc.plot_triple_view(M, save_name=save_name)
            if save_models:
                hi(string='saving model',N=i+1)
                np.save(save_dir_models+'M'+mcemc.number_string(N_iter, i+1), M)
                
        # Things we want everyone to do each iteration needs to happen here
        if check_mem:
            hi( string = 
               'checking memory '
               + str(i) + '\n'
               + os.popen('free -h').read() )
            
        if record_angle:
            if n_rank==0:
                angle_record_send = np.zeros((1),dtype=np.float64)
                #we're giving recv an extra place to receive crap from 0th proc
                angle_record_recv = np.empty( (N_shots * N_metro * 3 +1 ),
                                              dtype=np.float64 )
                len_local = np.array([1],dtype=np.int32 )
                lens = np.empty((N_cores),dtype=np.int32 )
            else:
                angle_record_send = angle_record_local.flatten()
                angle_record_recv = None
                len_local = np.array([N_shots_local*N_metro*3],dtype=np.int32 )
                lens = None
            comm.Gather(len_local,lens, root=0)
            hi(string='angle record Gatherv '+str(i)+' sending '+str(np.shape(angle_record_send)))
            comm.Gatherv( sendbuf = angle_record_send,
                          recvbuf=(angle_record_recv,lens), root=0)
            if n_rank ==0:
                print('angle record:')
                print('recv lens ',lens)
                print('recv shape ',np.shape(angle_record_recv))
                print('reshaped into ',(N_shots,N_metro,3))
                angle_record[i,:,:,:] = np.reshape(angle_record_recv[1:], (N_shots,N_metro,3) )
                
        if record_Js:
            if n_rank==0:
                J_record_send = np.zeros((1),dtype=np.float64)
                #we're giving recv an extra place to receive crap from 0th proc
                J_record_recv = np.empty( (N_shots * N_metro +1 ),
                                              dtype=np.float64 )
                len_local = np.array([1],dtype=np.int32 )
                lens = np.empty((N_cores),dtype=np.int32 )
            else:
                J_record_send = J_record_local.flatten()
                J_record_recv = None
                len_local = np.array([N_shots_local*N_metro],dtype=np.int32 )
                lens = None
            comm.Gather(len_local,lens, root=0)
            hi(string='J record Gatherv '+str(i)+' sending '+str(np.shape(J_record_send)))
            comm.Gatherv( sendbuf = J_record_send,
                          recvbuf=(J_record_recv,lens), root=0)
            if n_rank ==0:
                print('J record:')
                print('recv lens ',lens)
                print('recv shape ',np.shape(J_record_recv))
                print('reshaped into ',(N_shots,N_metro))
                J_record[i] = np.reshape(J_record_recv[1:], (N_shots,N_metro) )
                print('max J of each iteration\n',
                      np.around(np.amax(J_record,axis=(1,2)),2))
                
        if track_logl:
            if n_rank == 0:
                logl_tot = np.empty((1),dtype=np.float64)
                logl_subtot = np.zeros((1),dtype=np.float64)
            else:
                logl_tot = None
                logl_subtot = args['logl_subtot']
            comm.Reduce( logl_subtot, logl_tot, op = MPI.SUM, root = 0)
            if n_rank == 0:
                logls_ave[i]=(logl_tot/N_metro*N_shots)
                print('logls_ave',logls_ave)
                
    if n_rank == 0:
        hi( string = 'post iteration processes' )
        tf = time.time()
        if save_dir != '':
            make_info_file( save_dir, tf - ti )
            if record_angle:
                np.save(save_dir+'angle_record',angle_record)
            if record_Js:
                J0 = load.get_J0()
                np.save(save_dir+'J_record',J_record/J0)
            if save_plots:
                if track_logl:
                    mcemc.plot_logls(logls_ave, save_dir_plots=save_dir_plots)
            if save_models:
                np.save(save_dir_models+'M'+str(N_iter),M)
        print('complete')
                
    if redirect_std and save_dir != '':
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__
    comm.Barrier()
    
    
                              
 
