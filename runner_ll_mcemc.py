#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  6 13:30:42 2021

@author: Rainier
"""
import numpy as np
import time
from scipy.spatial.transform import Rotation as R
import configparser
import sys
import file_name
config_filename=sys.argv[1]
file_name.config_filename=config_filename
import loading_dragonfly_data as load
import ll_mcemc
import mcemc

cp=configparser.ConfigParser()
cp.read(config_filename)    

args = {
'N_edge' : int(cp['alg_params']['N_edge']),
'N_shots' : int(cp['alg_params']['N_shots']),
'N_iter' : int(cp['alg_params']['N_iter']),
'N_metro' : int(cp['alg_params']['N_metro']),
'N_burn' : int(cp['alg_params']['N_burn']),
'sig_rot_p' : np.float64(cp['alg_params']['sig_rot_p']),
'sig_J_on_J0' : np.float64(cp['alg_params']['sig_J_on_J0']),
'state_seed' : cp['alg_params']['state_seed']=='True',
'save_models' : cp['alg_params']['save_models']=='True',
'save_plots' : cp['alg_params']['save_plots']=='True',
'record_angle' : cp['alg_params']['record_angle']=='True',
'record_Js' : cp['alg_params']['record_Js']=='True',
'track_logl' : False,
'variable_J' : cp['alg_params']['variable_J']=='True',
        }
save_dir = ll_mcemc.make_save_dir(args)
ll_mcemc.ll_mcemc( args, save_dir = save_dir, redirect_std = False )
plot_dir = save_dir+'plots/'
models_dir  = save_dir+'models/'
M_mc=np.load(save_dir+'models/M'+mcemc.number_string(args['N_iter'],args['N_iter'])+'.npy')
M_mc = mcemc.regrid(M_mc,211,1,1)
M_best = load.get_M_theory()
M_best = mcemc.regrid(M_best,211,1,0.9915)
#q_to_best = R.from_rotvec([0,0,0]).as_quat()
q_to_best = ll_mcemc.orientation_match(M_best,M_mc,mask_r=1,N_it=2)
ll_mcemc.plot_results_ll(q_to_best,
                         M_mc,M_best,
                         plot_dir,models_dir,
                         mask_r = 0.8,clim=[6,12])

