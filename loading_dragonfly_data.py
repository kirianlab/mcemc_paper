#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 16:53:16 2021

@author: Rainier
"""

import read_data
import numpy as np
from scipy.spatial.transform import Rotation as R
import h5py
import mcemc
import configparser
import file_name

config_file = file_name.config_filename
cp = configparser.ConfigParser()
cp.read(config_file)
photon_file = cp['data_files']['photons']            
quats_file = cp['data_files']['quats']
M_theory_file = cp['data_files']['M_theory']

framegetter = read_data.DragonflyFrameGetter(photon_file, config_file)
# Setting up a reference frame
frame0 = framegetter.get_frame(0)
#print(frame0.keys())#if we want to see what's in the frame dict
pad_geometry = framegetter.pad_geometry
beam = framegetter.beam
q_vecs = pad_geometry.q_vecs( beam = beam )
J0 = beam.photon_number_fluence
E = beam.photon_energy
sa = pad_geometry.solid_angles()
pf = pad_geometry.polarization_factors(beam)
q_max = np.max( pad_geometry.q_mags( beam = beam ) )
N_frames_tot = framegetter.n_frames

# Loading discretized list of quats
quats_list= np.loadtxt(quats_file,skiprows=1)[:,:4]
N_rotations = len(quats_list)

def get_N_rotations():
    return N_rotations

def get_J0():
    return J0

def get_M_best(N_shots,N_edge,var_J=True):
    filename=('best_merges_given_params/'
              +'M_best_given'+str(N_shots)+'_edge'+str(N_edge))
    if not var_J:
        filename+='const_J'
    filename+='.npy'
    return np.load(filename)

def get_M_theory():
    file = M_theory_file 
    M=np.fromfile(file,dtype=np.float64)
    return M.reshape((211,211,211))

def axes_rot(rot90numz = 0 , zto = 'z'):
# returns a scipy rotation
# defaults to identity
    z_hat = np.array([0,0,1])
    R90z = R.from_rotvec( rot90numz * np.pi/2 * z_hat )
    
    if zto == 'z':
        vec_zto = np.array([0,0,0])
    elif zto == '-z':
        vec_zto = np.pi * np.array([1,0,0])
    elif zto == 'x':
        vec_zto = np.pi/2 * np.array([0,1,0])
    elif zto == '-x':
        vec_zto = -np.pi/2 * np.array([0,0,0])
    elif zto == 'y':
        vec_zto = -np.pi/2 * np.array([1,0,0])
    elif zto == '-y':
        vec_zto = np.pi/2 * np.array([1,0,0])
    
    Rzto = R.from_rotvec( vec_zto )
    Raxes = Rzto*R90z
    return Raxes


def load_orientation_tiles(n_start, n_end):
    return quats_list[n_start:n_end]
    

def load_rotations(n_start, n_end, rot90numz = 1 , zto = 'y', inv = False):
# loads the written out text file and returns the appropriate subset of them
#   with the appropriate axis rotation
#   correct looking merge is 1 'y' False
    quats_subset = quats_best[n_start:n_end]
    if inv:
        Rs_sci = R.from_quat(quats_subset).inv()
    else:
        Rs_sci = R.from_quat(quats_subset)
    Raxes = axes_rot(rot90numz = rot90numz , zto = zto)
    Rs_subset = Raxes * Rs_sci * Raxes.inv() 
    return Rs_subset

def load_Js(n_start, n_end):
    return Js_best[n_start:n_end]

def load_frames(n_start,n_end,check_mem=True):
    data = []
    print('loading ',n_end-n_start,' frames')
    for n in range(n_start,n_end):
        frame_n = framegetter.get_frame(n) 
        pad_data = frame_n['pad_data']
        pad_data = pad_geometry.concat_data(pad_data)
        data.append(pad_data)
    data = np.array(data)
    data_dict = { "pad_data": data,
                  "solid_angles": sa,
                  "polarization_factors": pf,
                  'q_vecs': q_vecs,
                  'q_vec_max': np.array([q_max,q_max,q_max]),
                  'J0': J0 }
    if check_mem:
        print('data size in gigabytes')
        print('data ',data.nbytes/1024**3)
        print('pf ',pf.nbytes/1024**3) 
        print('sa ',sa.nbytes/1024**3) 
        print('q_vecs ',q_vecs.nbytes/10**9) 
    return data_dict

def get_q_mags():
    return pad_geometry.q_mags( beam = beam )

def get_pad_geometry():
    return pad_geometry

def get_photon_energy():
    return E

def get_q_vecs():
    return q_vecs

def get_q_vec_max():
    return np.array([q_max,q_max,q_max])

