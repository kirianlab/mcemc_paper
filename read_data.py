import sys
import h5py
import numpy as np
import matplotlib.pyplot as plt
import pyqtgraph as pg
import configparser
from reborn.fileio.getters import FrameGetter
from reborn.viewers.qtviews import PADView2
from reborn.detector import PADGeometry, PADGeometryList
from reborn.source import Beam




class DragonflyFrameGetter(FrameGetter):
    def __init__(self, photon_file, config_file):
        super().__init__()
        cp = configparser.ConfigParser()
        cp.read(config_file)
        self.n_frames = int(cp['make_data']['num_data'])# This is necessary for making a FrameGetter subclass
        npix = int(cp['parameters']['detsize'])# number of pix along edge of square pad
        detd = float(cp['parameters']['detd'])*1e-3
        pixsize = float(cp['parameters']['pixsize'])*1e-3
        lam = float(cp['parameters']['lambda'])*1e-10
        photon_fluence = float(cp['make_data']['fluence'])*1e12# per square micron
        pulse_energy=np.pi*((1e-6)/2)**2*photon_fluence*6.626e-34*3e8/lam# pi x (d/2)^2 x fluence x h x c x lambda
        self.beam = Beam(wavelength=lam,pulse_energy=pulse_energy,diameter_fwhm=1e-6)
        f = h5py.File(photon_file, 'r')
        self.p1 = np.array(f['place_ones'])
        self.cm = np.array(f['count_multi'])
        self.pm = np.array(f['place_multi'])
        f.close()
        self.pad_geometry = PADGeometryList([PADGeometry(shape=(npix, npix), pixel_size=pixsize, distance=detd)])
        #print(self.beam)

    def get_data(self, frame_number=0):
        n = frame_number
        p1 = self.p1
        cm = self.cm
        pm = self.pm
        pat = np.zeros(self.pad_geometry[0].shape())
        pat.flat[p1[n]] = 1
        pat.flat[pm[n]] = cm[n]
        pat = [pat]
        return {'pad_data': pat}
    
'''
if __name__ == '__main__':
    
    #photon_file = 'data/photons.h5'
    #config_file = 'config.ini'
    photon_file = '../../dragonfly/recon_0001/data/photons.h5'
    config_file = '../../dragonfly/recon_0001/config.ini'
    
    fg = DragonflyFrameGetter(photon_file, config_file)
    pv = PADView2(frame_getter=fg)
    pv.start()
 ''' 
