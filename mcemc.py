#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 27 07:31:25 2021

@author: Rainier
"""

import time
import numpy as np
import scipy as sci
from reborn.target.density import trilinear_interpolation
from reborn.target.density import trilinear_insertion_factor, trilinear_insertion
from reborn.target.density import trilinear_insertions
from reborn.misc.rotate import Rotate3D
import matplotlib.pyplot as plt
from  scipy.spatial.transform import Rotation as R
import os
import datetime as dt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import loading_dragonfly_data as load

# constants
np.random.seed(0)
r_e = sci.constants.value('classical electron radius')
pi = np.pi
r_e2 = r_e**2
#cmap = 'Greys'
#cmap = 'plasma'_e
cmap = 'viridis'

# Tabulation of ln(N!)
lnfact = sci.special.gammaln( np.arange(1000000) + 1 )

# Book keeoing and plotting stuff -----------------------------------------------------
def sec_to_hrminsec_str(tf):
    if tf < 0.01:
        out = str(tf)+' sec'
    elif tf < 1:
        out = str( np.around( tf, 4 ) ) + ' sec'
    elif tf < 10:
        out = str( np.around( tf, 3 ) ) + ' sec'
    elif tf < 120:
        out = str( np.around( tf, 2 ) ) + ' sec'
    elif tf < 60**2: 
        out = str( tf // 60 ) + ' min ' + str( np.around( tf % 60, 1 ) ) + " sec"
    elif tf < 24*60**2:
        out = ( str( tf // 60**2 )+' hr '
              + str(( tf % 60**2 ) // 60 ) + " min "
              + str( np.around( ( tf % 60**2 ) % 60 ) )+' sec' )
    else:
        out = ( str( tf // ( 24 * 60**2 ) ) + ' days '
              + str( ( tf % ( 24 * 60**2 ) ) // 60**2 ) + ' hours '
              + str( ( ( tf % ( 24 * 60**2 ) ) % 60**2 ) // 60 ) + ' min '
              + str( np.around(  ( tf % ( 24*60**2 ) % 60**2 ) % 60 ) )
              + ' sec' ) 
    return out


def number_string(max_num,num):
    places = len(str(max_num))
    l = len(str(num))
    string = '0'*(places-l)+str(num)
    return string


#makes dir yr/mon/day/hr.min.sec
def make_date_dir():
    now_dt=dt.datetime.today()
    dirr = "plots/"
    if os.path.isdir(dirr) == False:
        os.mkdir(dirr)
    dirr += number_string(2222,now_dt.year)+'/'
    if os.path.isdir(dirr) == False:
        os.mkdir(dirr)
    dirr += number_string(12,now_dt.month)+"/"
    if os.path.isdir(dirr) == False:
        os.mkdir(dirr)
    dirr += number_string(31,now_dt.day)+"/"
    if os.path.isdir(dirr) == False:
        os.mkdir(dirr)
    dirr += number_string(24, now_dt.hour)+'.'
    dirr += number_string(60, now_dt.minute)+"."
    dirr += number_string(60, now_dt.second) + '/'
    if os.path.isdir(dirr) == False:
        os.mkdir(dirr)
    return dirr


def colorbar_ax(ax, im, label = None):
    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    if label == None:
        plt.colorbar(im, cax=cax)
    else:
        plt.colorbar( im, cax=cax, label = label ) 


def imshow_ln(ar, clim=None, save_name=None, colorbar=True, ax=None):
    x = ar[ int(np.floor( len( ar )*0.5 )) ]       
    if ax == None:
        plt.imshow( np.log10( x ), cmap = cmap, clim=clim, interpolation='none')
        if colorbar:
            plt.colorbar().set_label('Log10 intensity')
        if save_name != None:
             plt.savefig(save_name)
             plt.clf()
        else:
            plt.show()
    else:
        im = ax.imshow( np.log10(x), cmap = cmap, clim=clim )
        if colorbar:
            colorbar_ax( ax, im )
       

def plot_triple_view(ar,save_name=None,colorbar=True,axes=None,clim=None):
    l = int(np.floor( len( ar )*0.5 ))
    x = ar[:,:,l]
    y = ar[:,l,:]
    z = ar[l,:,:]
    v = [x,y,z]
    
    if axes is None:
        fig, axes_ = plt.subplots(1,3)
    else:
        axes_=axes
    for i in range(3):
        axes_[i].imshow( np.log10(v[i]), cmap = cmap, clim=clim )
        axes_[i].set_axis_off()
    if axes is None:
        if save_name != None:
             fig.savefig(save_name)
        else:
            plt.show()
            
def plot_triple_progression(save_dir_models,N_show,save_name=None, clim=None,mask_r=None):
    col=3
    row=N_show
    fig = plt.figure( figsize = (col,row) )
    gs = fig.add_gridspec( row, col )
    axes_list = []
    for n in range(row):
        axes_sublist=[]
        for i in range(col):
            ax__ = fig.add_subplot( gs[ n:n+1, i:i+1 ] )
            axes_sublist.append(ax__)
        axes_list.append(axes_sublist)
        
    files = np.sort(os.listdir(save_dir_models))
    N_max = len(files)-1
    r = np.exp(np.log(N_max)/(N_show-1))
    if mask_r is not None:
        M_ = np.load(save_dir_models+files[0])
        N_edge=M_.shape[0]
        w = np.where(sphere_mask(mask_r,N_edge)==0)
    for n in range(N_show):
        n_ = int(np.round(r**n))
        M = np.load(save_dir_models+files[n_])
        if mask_r is not None:
            M[w]=0
        axes = axes_list[n]
        plot_triple_view(M,axes=axes,clim=clim)
        axes[0].set_xlabel(str(n))
    if save_name is not None:
        fig.savefig(save_name)
    else:
        plt.show()

def plot_progression(M_best=None,save_dir_models=None,save_name=None,clim=None,mask_r=None):
    plt.rc( 'axes', titlesize = 5*3 )     # fontsize of the axes title
    col=3*3
    row=2*3
    fig = plt.figure( figsize = (col,row) )
    gs = fig.add_gridspec( row, col )
    axes_list = []
    for n in range(6):
        i=int(n/3)
        j=int(n%(col/3))
        ax__ = fig.add_subplot( gs[ i*3:(i+1)*3, j*3:(j+1)*3 ] )
        axes_list.append(ax__)
    if save_dir_models is not None:
        files = np.sort(os.listdir(save_dir_models))
        N_max = len(files)-1
    else:
        N_max=32
    r = np.exp(np.log(N_max)/(6-1))
    if mask_r is not None:
        if save_dir_models is not None:
            M_ = np.load(save_dir_models+files[0])
            N_edge=M_.shape[0]
            w = np.where(sphere_mask(mask_r,N_edge)==0)
        elif M_best is not None:
            M_=load.load_dragonfly_merge(0)
            N_edge=M_.shape[0]
            mask = sphere_mask(mask_r,N_edge)
            w = np.where(mask==0)
            if M_best is not None:
                if M_best.shape[0]!=N_edge:
                    M_best=regrid(M_best, N_edge, 1, 1)
            c = model_normalize(M_,M_best,mask)
            M_ *= c
    for n in range(6):
        n_ = int(np.round(r**n))
        if save_dir_models is None:
            M = load.load_dragonfly_merge(n_)
            if mask_r is None:
                mask=sphere_mask(1, N_edge)
            c = model_normalize(M,M_best,mask)
            M *= c
        else:
            M = np.load(save_dir_models+files[n_])
        if mask_r is not None:
            M[w]=0
        ax = axes_list[n]
        imshow_ln(M,ax=ax,colorbar=False,clim=clim)
        ax.set_axis_off()
        ax.set_title(str(n_),y=1,pad=-4*3)
    if save_name is not None:
        fig.savefig(save_name)
    else:
        fig.show()
      
def plot_model_diff(save_dir_models,M_best,mask,save_dir_plots=None,ax=None):
    files=np.sort(os.listdir(save_dir_models))
    l = len(files)
    N_edge=M_best.shape[0]
    M_ = np.load(save_dir_models+files[0])
    if M_.shape[0]!=N_edge:
            M_ = regrid(M_,N_edge,1,1)
    c = model_normalize(M_,M_best,mask)
    M_ *= c
    diff = np.empty((l-1))
    for i in range(l)[1:]:
        M = np.load(save_dir_models+files[i])
        if M.shape[0]!=N_edge:
            M = regrid(M,N_edge,1,1)
        c = model_normalize(M,M_best,mask)
        M *= c
        diff[i-1] = np.sqrt(np.mean((M-M_)**2)) / (np.sum(M+M_)/2)
        M_ = M.copy()
    diff = np.log10(diff)
    
    if ax == None:
        plt.plot(diff)
        if save_dir_plots != None:
            plt.savefig(save_dir_plots+'differences.pdf')
            plt.clf()
    else:
        ax.plot(diff,label='Monte Carlo')
    
    
def plot_model_diff_dragonfly(N_it_drag,M_best,mask,save_dir_plots=None,ax=None):
    N_edge = M_best.shape[0]
    l = N_it_drag
    M_ = load.load_dragonfly_merge(0)
    if M_.shape[0]!=N_edge:
            M_ = regrid(M_,N_edge,1,0.982146)
    c = model_normalize(M_,M_best,mask)
    M_ *= c
    diff = np.empty((l-1))
    for i in range(l)[1:]:
        M = load.load_dragonfly_merge(i)
        if M.shape[0]!=N_edge:
            M = regrid(M,N_edge,1,0.982146)
        c = model_normalize(M,M_best,mask)
        M *= c
        diff[i-1] = np.sqrt(np.mean((M-M_)**2)) / (np.sum(M+M_)/2)
        M_ = M.copy()
    diff = np.log10(diff)
    
    if ax == None:
        plt.plot(diff)
        if save_dir_plots != None:
            plt.savefig(save_dir_plots+'differences_dragonfly.pdf')
            plt.clf()
    else:
        ax.plot(diff,label='Dragonfly',linestyle='--')
     
        
def plot_model_err(M_best,q_best,mask,save_dir_models,save_dir_plots=None,ax=None):
    files=np.sort(os.listdir(save_dir_models))
    l = len(files)
    errs = np.empty(l,dtype=np.float64)
    N_edge=np.shape(M_best)[0]
    for i in range(l):
        M = np.load(save_dir_models+files[i])
        if M.shape[0]!=N_edge:
            M = regrid(M,N_edge,1,1)
        err = calc_error(q_best,M_best,M,mask)
        errs[i] = err
    s = np.sqrt(((M_best*mask)**2).sum())
    errs/=s
    errs  = np.log10(errs)  
    if ax == None:
        plt.plot(errs)
        if save_dir_plots != None:
            plt.savefig(save_dir_plots+'errors.pdf')
            plt.clf()
    else:
        ax.plot(errs,label='Monte Carlo')
        
def plot_model_err_dragonfly(M_best,q_best_drag,mask,N_it_drag,save_dir_plots=None,ax=None):
    errs = np.empty(N_it_drag,dtype=np.float64)
    N_edge=np.shape(M_best)[0]
    for i in range(N_it_drag):
        M = load.load_dragonfly_merge(i)
        if M.shape[0]!=N_edge:
            M = regrid(M,N_edge,1,1)
        err = calc_error(q_best_drag,M_best,M,mask)
        errs[i] = err
    s = np.sqrt(((M_best*mask)**2).sum())
    errs/=s
    errs  = np.log10(errs)  
        
    if ax == None:
        plt.plot(errs)
        if save_dir_plots != None:
            plt.savefig(save_dir_plots+'errors_dragonfly.pdf')
            plt.clf()
    else:
        ax.plot(errs,label='Dragonfly',linestyle='--')
    
    
def plot_logls(logls_ave, save_dir_plots=None, ax=None):
    logls_ave/=np.log(10)
    if ax == None:
        plt.plot(logls_ave)
        if save_dir_plots != None:
            plt.savefig(save_dir_plots+'ave_log_likelihoods.pdf')
            plt.xlabel('Iterations')
            plt.ylabel('log10 likelihood')
            plt.grid()
            plt.clf()
    else:
        ax.plot(logls_ave)
        ax.set_xlabel('Iterations')
        ax.set_ylabel('log10 likelihood')
        
def plot_rotated_models_both(M_mcemc,M_drgn,M_best,
                             q_mc_to_best,q_drgn_to_best,mask,
                             save_dir_plots=None,axes=None,clim=None):
    
    R_mc_to_best = R.from_quat(q_mc_to_best)
    R_drgn_to_best = R.from_quat(q_drgn_to_best)
    M_mc_rot=rotate_density_map2(M_mcemc,R_mc_to_best)
    M_drgn_rot=rotate_density_map2(M_drgn,R_drgn_to_best)
    c = model_normalize(M_drgn_rot,M_best,mask)
    M_drgn_rot *= c
    #M_best[M_best==0]=M_best.mean()
    #M_drgn_rot[M_drgn_rot==0]=M_drgn_rot.mean()
    
    w=np.where(mask==0)
    M_mc_rot[w]=0
    M_drgn_rot[w]=0
    M_best[w]=0
    
    if clim is None:
        max_ = np.log10(M_best.max())
        min_ = np.log10(M_best.min())
        clim = [min_,max_]
    if axes is None:
        fig, axes = plt.subplots(1,3)

    imshow_ln( M_mc_rot, ax=axes[0], clim=clim)
    axes[0].set_axis_off()
    
    imshow_ln( M_best, ax=axes[1], clim=clim)
    axes[1].set_axis_off()
    
    imshow_ln( M_drgn_rot, ax=axes[2], clim=clim)
    axes[2].set_axis_off()
    
    if save_dir_plots is not None:
        plt.subplots_adjust( wspace = 0.3, hspace = 0.35 )
        plt.savefig(save_dir_plots+'rotated_models.pdf')
        plt.clf()


def plot_rotated_model_to_best(M_mcemc,M_best,
                             q_mc_to_best,mask,
                             save_dir_plots=None,axes=None,clim=None):
    
    w=np.where(mask==0)
    R_mc_to_best = R.from_quat(q_mc_to_best)
    M_mc_rot=rotate_density_map2(M_mcemc,R_mc_to_best)
    M_mc_rot[w]=0
    M_best[w]=0
    
    if clim is None:
        max_ = np.log10(M_best.max())
        min_ = np.log10(M_best.min())
        clim = [min_,max_]
    if axes is None:
        fig, axes = plt.subplots(1,2)

    imshow_ln( M_mc_rot, ax=axes[0], clim=clim)
    axes[0].set_axis_off()
    
    imshow_ln( M_best, ax=axes[1], clim=clim)
    axes[1].set_axis_off()
    
    if save_dir_plots is not None:
        plt.subplots_adjust( wspace = 0.3, hspace = 0.35 )
        plt.savefig(save_dir_plots+'rotated_model.pdf')
        plt.clf()
    
            
def plot_angle_record_histograms(angle_record,n,plot_dir):
    name = plot_dir+'angle_hist_'+str(n+1)+'.pdf'
    a = angle_record[n+1]
    w = np.where(a>pi/2)
    a[w]=pi-a[w]
    a = angle_record[n+1].flatten()
    w = np.where(a>pi/2)
    a[w]=pi-a[w]
    plt.hist(a.flatten(),bins=50)
    plt.savefig(name)
    plt.clf()
    
            
def plot_J_ave(J_record,n,plot_dir):
    # shape is (N_iter,N_shots,N_metro)
    name = plot_dir+'angle_hist_'+str(n+1)+'.pdf'
  
    plt.savefig(name)
    plt.clf()
    
    
def plot_results(M_mcemc,M_drgn,M_best,q_mc_to_best,q_drgn_to_best,
                 N_it_drag,mask,models_dir,plots_dir,clim=None):
    
    plt.rc( 'axes', titlesize = 12 )    # fontsize of the axes title
    plt.rc( 'axes', labelsize = 10 )    # fontsize of the x and y labels
    plt.rc( 'xtick', labelsize = 8 )    # fontsize of the tick labels
    plt.rc( 'ytick', labelsize = 8 )    # fontsize of the tick labels
    plt.rc( 'legend', fontsize = 8 )    # legend fontsize
    plt.rc( 'figure', titlesize = 14 )   # fontsize of the figure title

    row=7
    col=11
    fig = plt.figure( figsize = (col,row) )
    gs = fig.add_gridspec( row, col )
 
    ax00 =fig.add_subplot( gs[ 0:3, 0:3 ] )
    ax01 =fig.add_subplot( gs[ 0:3, 4:7 ] )
    ax02 = fig.add_subplot( gs[ 0:3, 8:11 ] ) 
    ax10 =fig.add_subplot( gs[ 4:7, 0:5 ] )
    ax11 =fig.add_subplot( gs[ 4:7, 6:11 ] )

    ax00.set_title('(a)',y=1,pad=-12)
    ax01.set_title('(b)',y=1,pad=-12)
    ax02.set_title('(c)',y=1,pad=-12)
    ax10.set_title('(d)')
    ax11.set_title('(e)')
    
    plot_rotated_models_both(M_mcemc, M_drgn, M_best,
                             q_mc_to_best, q_drgn_to_best,mask,
                             axes = [ax00,ax01,ax02],clim=clim)
    
    plot_model_err(M_best,q_mc_to_best,mask,models_dir,ax=ax11)
    plot_model_err_dragonfly(M_best,q_drgn_to_best,mask,N_it_drag,ax=ax11 )
    ax11.set_xlabel('Iterations')
    ax11.set_ylabel('log10 relative RMS error')
    ax11.grid()
    ax11.legend()
    
    plot_model_diff(models_dir,M_best,mask,ax=ax10)
    plot_model_diff_dragonfly(N_it_drag,M_best,mask,ax=ax10)
    ax10.set_xlabel('Iterations')
    ax10.set_ylabel('log10(\u0394M)')
    ax10.grid()
    ax10.legend()
        
    fig.savefig(plots_dir+'results.pdf')
   

def plot_result_mc(M_mcemc,M_best,q_mc_to_best,                       
                   mask,models_dir,plot_dir,clim=None):        
    
    plt.rc( 'axes', titlesize = 12 )    # fontsize of the axes title
    plt.rc( 'axes', labelsize = 10 )    # fontsize of the x and y labels
    plt.rc( 'xtick', labelsize = 8 )    # fontsize of the tick labels
    plt.rc( 'ytick', labelsize = 8 )    # fontsize of the tick labels
    plt.rc( 'legend', fontsize = 8 )    # legend fontsize
    plt.rc( 'figure', titlesize = 14 )  # fontsize of the figure title

    row=7
    col=11
    fig = plt.figure( figsize = (col,row) )
    gs = fig.add_gridspec( row, col )
 
    ax00 =fig.add_subplot( gs[ 0:5, 0:5 ] )
    ax01 =fig.add_subplot( gs[ 0:5, 6:11 ] )
    ax10 =fig.add_subplot( gs[ 5:7, 0:5 ] )
    ax11 =fig.add_subplot( gs[ 5:7, 6:11 ] )

    ax00.set_title('(a)',y=1,pad=-12)
    ax01.set_title('(b)',y=1,pad=-12)
    ax10.set_title('(d)')
    ax11.set_title('(e)')

    plot_rotated_model_to_best(M_mcemc,M_best,
                               q_mc_to_best,mask,
                               axes = [ax00,ax01],clim=clim)
    
    plot_model_err(M_best,q_mc_to_best,mask,models_dir,ax=ax11)
    ax11.set_xlabel('Iterations')
    ax11.set_ylabel('log10 relative RMS error')
    ax11.grid()
    ax11.legend()
    
    plot_model_diff(models_dir,M_best,mask,ax=ax10)
    ax10.set_xlabel('Iterations')
    ax10.set_ylabel('log10(\u0394M)')
    ax10.grid()
    ax10.legend()
        
    fig.savefig(plot_dir+'results.pdf')
 

def rotate_density_map(rho,R_):
    #we need to turn [i,j,k] into q-coord vectors to rotate them
    #truncated to the ball r >=qmax to avoid going out of bounds
    q_vec_max = np.array([1,1,1])
    q_vec_min = -q_vec_max
    print('doing slow roation')
    print('q_vec_min ',q_vec_min,', q_vec_max ',q_vec_max,)
    N_edge = np.shape(rho)[0]
    dx = (q_vec_max[0] - q_vec_min[0])/(N_edge-1)
    
    indices_thing = np.empty((N_edge,N_edge,N_edge,3))
    for i in range(N_edge):
        indices_thing[i,:,:,0] = np.full((N_edge,N_edge),i)
    for i in range(N_edge):
        indices_thing[:,i,:,1] = np.full((N_edge,N_edge),i)
    for i in range(N_edge):
        indices_thing[:,:,i,2] = np.full((N_edge,N_edge),i)
    
    q_vecs_ = indices_thing*dx+q_vec_min
    q_mags_ = np.sum( q_vecs_**2, axis = 3 )
    w = np.where(q_mags_ <= q_vec_max[0])
    q_vecs = q_vecs_[w]
    q_vecs = np.dot( q_vecs, R_.as_matrix().T )
    vals = rho[w]
    densities = np.zeros((N_edge,N_edge,N_edge))
    weights = np.zeros((N_edge,N_edge,N_edge))
    
    trilinear_insertion(densities, weights,
                         q_vecs, vals,
                         x_min=q_vec_min, x_max=q_vec_max)
    
    num = densities
    dnm = weights
    w_not_zero = np.where( dnm != 0 )# avoiding dividing by zero
    M = num.copy()
    M[w_not_zero] /= dnm[w_not_zero]# division converting to weighted averaged
    w_zero = np.where( M == 0 )# finding zero voxels
    M[w_zero] = np.mean(M)# and filling with the mean
    ave_rho = np.mean(rho[w])# scale of values is ridulously off so
    ave_M = np.mean(M[w])#       scale is forced to agree
    return M*ave_rho/ave_M


def rotate_density_map2(rho,R_):
# this one interpolates at a rotated point
    q_vec_max = np.array([1,1,1])
    q_vec_min = -q_vec_max
    N_edge = np.shape(rho)[0]
    dx = (q_vec_max[0] - q_vec_min[0])/(N_edge-1)
    
    indices_thing = np.empty((N_edge,N_edge,N_edge,3))
    for i in range(N_edge):
        indices_thing[i,:,:,0] = np.full((N_edge,N_edge),i)
    for i in range(N_edge):
        indices_thing[:,i,:,1] = np.full((N_edge,N_edge),i)
    for i in range(N_edge):
        indices_thing[:,:,i,2] = np.full((N_edge,N_edge),i)
    
    q_vecs_ = indices_thing*dx+q_vec_min
    q_mags_ = np.sum( q_vecs_**2, axis = 3 )
    w = np.where(q_mags_ <= q_vec_max[0])
    q_vecs = q_vecs_.reshape((N_edge**3,3))
    q_vecs = np.dot( q_vecs, R_.as_matrix().T )
    vals = trilinear_interpolation( rho, q_vecs, x_min = q_vec_min, x_max = q_vec_max )
    w = np.where(q_mags_ <= q_vec_max[0])
    anti_mask = np.ones((N_edge,N_edge,N_edge))
    anti_mask[w] = 0 
    M = vals.reshape(np.shape(rho))
    M[anti_mask==1]=np.mean(M*(1-anti_mask))
    return M

def regrid(M_in,N_edge,q_face_in,q_face_new):
# this one interpolates at a rotated point
    q_vec_max_new = q_face_new*np.array([1,1,1])
    q_vec_min_new = -q_vec_max_new
    dq = 2*q_face_new/(N_edge-1)
    
    indices_thing = np.empty((N_edge,N_edge,N_edge,3))
    for i in range(N_edge):
        indices_thing[i,:,:,0] = np.full((N_edge,N_edge),i)
    for i in range(N_edge):
        indices_thing[:,i,:,1] = np.full((N_edge,N_edge),i)
    for i in range(N_edge):
        indices_thing[:,:,i,2] = np.full((N_edge,N_edge),i)
    
    q_vecs_ = indices_thing*dq+q_vec_min_new
    q_vecs = q_vecs_.reshape((N_edge**3,3))
    q_vec_max_in = q_face_in*np.array([1,1,1])
    q_vec_min_in = -q_vec_max_in
    vals = trilinear_interpolation( M_in, q_vecs, x_min = q_vec_min_in, x_max = q_vec_max_in )
    M = vals.reshape((N_edge,N_edge,N_edge))
    return M


def sphere_mask(r,N_edge):
# returns a 3d array of ones inside of r zeroes outside
# r ratio to max q value
# r = one makes a sphere inscribed in the cube (tangent to faces)
    if r<0:
        r = 0.0
    q_vec_max = np.array([1,1,1])
    q_vec_min = -q_vec_max
    dx = (q_vec_max[0] - q_vec_min[0])/(N_edge-1)
    
    indices_thing = np.empty((N_edge,N_edge,N_edge,3))
    for i in range(N_edge):
        indices_thing[i,:,:,0] = np.full((N_edge,N_edge),i)
    for i in range(N_edge):
        indices_thing[:,i,:,1] = np.full((N_edge,N_edge),i)
    for i in range(N_edge):
        indices_thing[:,:,i,2] = np.full((N_edge,N_edge),i)
    
    q_vecs_ = indices_thing*dx+q_vec_min
    q_mags_ = np.sum( q_vecs_**2, axis = 3 )
    w = np.where(q_mags_ <= r**2)
    mask = np.zeros((N_edge,N_edge,N_edge))
    mask[w] = 1 
    return mask


def model_normalize(M,M_best,mask):
    M_best=M_best*mask
    M=M*mask
    a = np.sum(M_best*M)/np.sum(M**2)
    return a
    
def calc_error(q,M1,M2,mask):
    R_ = R.from_quat(q)
    M1 = mask*M1
    M2_= mask*rotate_density_map2(M2,R_)
    a = np.sum(M1*M2_)/np.sum(M2_**2)
    diff = mask*(M1-a*M2_)
    err = np.sqrt(np.sum(diff**2))
    return err
    


    

# Crucial functions -------------------------------------------------------

def data_ewald_sum(data_dict):
    q_mags = load.get_q_mags()
    pad_geometry = load.get_pad_geometry()
    q_mags2 = np.reshape(q_mags,(pad_geometry[0].n_ss,pad_geometry[0].n_fs))
    q_edge = q_mags2[0,int(pad_geometry[0].n_fs/2)]
    sa = data_dict['solid_angles']
    pf = data_dict['polarization_factors']
    pad_data = data_dict['pad_data']
    w = np.where(q_mags<=q_edge)
    mean = 0.0
    for shot in pad_data:
        mean += np.mean(shot[w]/(pf[w]*sa[w]))
    J0 = data_dict['J0']
    return mean/(J0*r_e2)


def model_ewald_mean(M):
    N_edge = np.shape(M)[0]
    q_vec_max = load.get_q_vec_max()
    q_vec_min = -q_vec_max
    dx = (q_vec_max[0] - q_vec_min[0])/(N_edge-1)

    q_mags = load.get_q_mags()
    pad_geometry = load.get_pad_geometry()
    q_mags2 = np.reshape(q_mags,(pad_geometry[0].n_ss,pad_geometry[0].n_fs))
    q_edge = q_mags2[0,int(pad_geometry[0].n_fs/2)]
    
    indices_thing = np.empty((N_edge,N_edge,N_edge,3))
    for i in range(N_edge):
        indices_thing[i,:,:,0] = np.full((N_edge,N_edge),i)
    for i in range(N_edge):
        indices_thing[:,i,:,1] = np.full((N_edge,N_edge),i)
    for i in range(N_edge):
        indices_thing[:,:,i,2] = np.full((N_edge,N_edge),i)
    
    q_vecs_ = indices_thing*dx+q_vec_min
    q_mags_ = np.sqrt(np.sum( q_vecs_**2, axis = 3 ))
    w_ = np.where((q_mags_<=q_edge)*(q_mags_>q_edge/10000))
    mean_ = 1/(2*pi*q_edge**2)*dx**3*np.sum(M[w_]/q_mags_[w_])
    return mean_


def interpolate( M, vecs, q_vec_min, q_vec_max ):
# Trilinear interpolation of the density M on a grid.
#     q_vec_min and q_vec_max tell it the conversion from voxel index to q-vector.
#     outputs an array with a value for each vec in vecs
    return trilinear_interpolation( M, vecs, x_min = q_vec_min, x_max = q_vec_max )


def insert( arr2, vecs, vals, q_vec_min, q_vec_max, weight_factor = 1 ):
# Trilinear insertion of vals into a 3D array.
#     arr2 is edited by the function and has a two N_edge**3 arrays;
#       one is the numerator and is one the denominator of the insertion.
#     q_vec_min and q_vec_max tell it the conversion from voxel index to q-vector.
#     Weight factor multiplies the denominator array.
    return trilinear_insertion_factor(densities = arr2,
                                      weight_factor = weight_factor,
                                      vectors = vecs,
                                      insert_vals = vals,
                                      x_min = q_vec_min,
                                      x_max = q_vec_max)


def insert2(arr2, vecs, vals1, vals2, q_vec_min, q_vec_max):
    densities1 = arr2[:,:,:,0].copy()
    densities2 = arr2[:,:,:,1].copy()
    weights1 = np.ones_like(densities1)
    weights2 = np.ones_like(densities2)
    
    trilinear_insertion(densities1, weights1,
                        vecs, vals1,
                        x_min=q_vec_min, x_max=q_vec_max)
    
    trilinear_insertion(densities2, weights2,
                        vecs, vals2,
                        x_min=q_vec_min, x_max=q_vec_max)
    
    arr2[:,:,:,0] = densities1
    arr2[:,:,:,1] = densities2
    


def gauss( x, mu = 0, sig = 1 ):
# Normalized gaussian
    out = ( 1 / ( sig * np.sqrt( 2*pi ) )
           * np.exp( (-1/2) * (x - mu)**2 / sig**2 ) )
    return out    


def lnfactorial(arr):
# calls lookup table lnfact
    return lnfact[arr.astype(np.int)]


def log_likelihood( M, pad_data, q_vecs, solid_angles, J, q_vec_min, q_vec_max ):
    lamb = interpolate( M, q_vecs, q_vec_min, q_vec_max )
    lamb *= solid_angles * J * r_e2
    k = pad_data
    lnp = k * np.log( lamb ) - lamb - lnfactorial( k )
    return np.sum( lnp )

def log_prior_J(J,J0):
    x = J/J0
    #np.log(4*x*np.exp(-2*x))
    return np.log(4*x)-2*x

def sample_gauss_periodic(sig):
# Sampling a truncated gaussian in a dumb way.
#     Returns angles from -pi to pi.
    phi = sig * np.random.standard_normal()
    while phi > pi or phi < - pi:
        phi = sig * np.random.standard_normal()
    return phi


def random_unit_vec():
# Returns a uniformly random unit vector.
    n_vec = np.array( [np.random.normal(),
                       np.random.normal(),
                       np.random.normal()] )  #random direction
    n_vec /= np.sqrt( np.sum( n_vec**2 ) )    #normalized
    return n_vec


def random_R():
    n_vec = random_unit_vec()
    phi = np.random.uniform(low = - pi, high = pi)
    return R.from_rotvec( phi * n_vec )


def fill_angle(angle_record_vec,count,metro_index,R_state,k):
# fills angle_record_vec with angle squared
# the shape is (N_shots_local,N_metro,3)
    angle_record_vec[k,metro_index+1-count:metro_index+1,:] = R_state.as_rotvec()
  

def fill_Js(J_record_vec,count,metro_index,J_state,k):
    J_record_vec[k,metro_index+1-count:metro_index+1] = J_state
                
                
def M_from_arr2(arr2, fill_zeros = True):
# Creates a (N,N,N) model with a (2,N,N,N) of inserts intensities and weights 
        num = arr2[:,:,:,0]
        dnm = arr2[:,:,:,1]
        w = np.where( dnm != 0 )# avoiding dividing by zero
        M = num.copy()
        M[w] /= dnm[w]# division converting to weighted averaged
        if fill_zeros:
            w_zero = np.where( M == 0 )# finding zero voxels
            N_edge = np.shape(num)[0]
            mask=sphere_mask(1,N_edge)
            M[w_zero] = np.mean(M[mask==1])# and filling with the mean
        return M
      
    
def update_M_with_arr2(M,arr2,rotation_ave=True):
# overwrites M based on arr2
    num = arr2[:,:,:,0]
    dnm = arr2[:,:,:,1]
    if not rotation_ave:
        w = np.where( dnm * num != 0 )
        M[w] = num[w]/dnm[w]# division converting to weighted averaged
    else:
        w = np.where( dnm != 0 )
        M[w] = num[w]/dnm[w]
        mask_zero = np.zeros_like(M)
        mask_zero[M==0] = 1
        mask_zero[dnm==0] = 1
        mask_dnm = np.zeros_like(M)
        mask_dnm[dnm==0] = 1
        mask_not_zero=1-mask_zero
        N_edge = np.shape(M)[0]
        delta = np.sqrt(2)/(N_edge/2-1/2+int(N_edge%2))
        rs = delta*(1/2 + int(N_edge%2)/2 + np.arange(int(N_edge/2)))
        for r in rs:
            mask_delta_r = sphere_mask(r+delta/2, N_edge)-sphere_mask(r-delta/2, N_edge)
            w_not_zero_delta = np.where((mask_not_zero*mask_delta_r)==1)
            if w_not_zero_delta[0].size != 0:
                M[(mask_zero*mask_delta_r)==1]= M[w_not_zero_delta].mean()
            else:
                M[(mask_zero*mask_delta_r)==1]= M[(mask_delta_r*mask_dnm)==1].mean()
                
    
    

# Merge given Rs & Js---------------------------------------------------------------

def reconstruction_given_Rs_Js_ll( Rs, Js, data_dict, args ):
# putting all the diffraction patterns into arr2 at the given Rs and Js
#   edits arr2

    # unpacking variables
    data = data_dict['pad_data']
    vecs_da = data_dict['q_vecs']
    solid_angles = data_dict['solid_angles'] 
    q_vec_max = data_dict['q_vec_max']
    q_vec_min = -q_vec_max
    N_edge = args['N_edge']
    
    arr2 = np.zeros( [N_edge, N_edge, N_edge, 2] )
    vals2 = np.empty((len(data[0]),2))
    Js_ = r_e2 * Js
    for k in range(len( data )):
        vecs = np.dot( vecs_da, Rs[k].as_matrix().T )
        vals2[:,0] = data[k]
        vals2[:,1] = Js_[k] * solid_angles
        trilinear_insertions(arr2,vecs,vals2,x_min=q_vec_min,x_max=q_vec_max)
    return arr2

 
# MCEMC loops---------------------------------------------------------------

def shot_loop_ll(arr2, M, data_dict, args,
                 angle_record_vec = None):
# The loop over the diffraction patterns
#   Acts on arr2
   
    # defining variables
    shots_pad_data = data_dict['pad_data']
    N_shots_local = len(shots_pad_data)
    N_metro = args['N_metro']
    N_burn = args['N_burn']
    sig_rot_p = args['sig_rot_p']
    sig_J_on_J0 = args['sig_J_on_J0']
    state_seed = args['state_seed']
    record_angle = args['record_angle'] 
    record_Js = args['record_Js']
    track_logl = args['track_logl'] 
    variable_J = args['variable_J']
    vecs_da = data_dict['q_vecs']
    solid_angles = data_dict['solid_angles'] 
    q_vec_max = data_dict['q_vec_max']
    q_vec_min = -q_vec_max
    J0 = data_dict['J0']
    sig_J_p = J0 * sig_J_on_J0
    
    # shorting the log_likelihood notation
    def logl ( M, K_k, vecs_da_rot, J ):
        return log_prior_J(J,J0) + log_likelihood( M, K_k, vecs_da_rot, solid_angles,
                                                   J, q_vec_min, q_vec_max )
    
    def inserts(vecs_da_state, count, J_state, vals2):
        vals2[:,0] = count * K_k
        vals2[:,1] = count * r_e2 * J_state * solid_angles
        trilinear_insertions(arr2,vecs_da_state,vals2,x_min=q_vec_min,x_max=q_vec_max )
        
    if track_logl:
        logl_tot = 0
    for k in range( N_shots_local ):
        
        K_k = shots_pad_data[k]
        vals2 = np.empty((len(K_k),2))# allocating memory
             
        if state_seed:
            R_state = args['Rs_state'][k]
            J_state = args['Js_state'][k]
        else:
            R_state = random_R()
            if variable_J:
                J_state = J0*np.abs(1 + sig_J_on_J0 * np.random.standard_normal())
            else:
                J_state = J0
        vecs_da_state = np.dot( vecs_da, R_state.as_matrix().T ) 
        logl_state = logl( M, K_k, np.dot( vecs_da, R_state.as_matrix().T ) , J_state )
        
        for n_metro in range(N_metro+N_burn):
                if n_metro == N_burn:
                        count = 1
                if variable_J:
                    J_p = J_state + sig_J_p * np.random.standard_normal()# proposed J
                else:
                    J_p = J0
                if J_p > 0:
                    n_vec = random_unit_vec()
                    phi = sample_gauss_periodic( sig_rot_p )
                    R_p = R.from_rotvec(phi*n_vec) * R_state# proposed rotation
                    vecs_da_p = np.dot( vecs_da, R_p.as_matrix().T )# proposed q-vecs
                    logl_p = logl( M, K_k, vecs_da_p, J_p )# proposed log likelihood
                    logA = np.min([0,(logl_p-logl_state)])# log acceptance ratio
                    u = np.random.uniform()
                    if np.log( u ) <= logA:# acceptance condition
                        if n_metro >= N_burn:
                            inserts(vecs_da_state, count, J_state, vals2)
                            if record_angle:# assigning a chunk of the angle record
                                fill_angle(args['angle_record_arr'],
                                           count,n_metro-N_burn,R_state,k)
                            if record_Js:# assigning a chunk of the J record
                                fill_Js(args['J_record_arr'],
                                        count,n_metro-N_burn,J_state,k)
                            if track_logl:
                                logl_tot += count*logl_state
                            count = 0# reseting the rejection count
                        R_state = R_p# orientation move
                        J_state = J_p# fluence move
                        vecs_da_state = vecs_da_p
                        logl_state = logl_p
                if n_metro >= N_burn:
                    count += 1# ticking up count
        count -= 1
        inserts(vecs_da_state, count, J_state, vals2)
        if record_angle:# assigning last chunk of the angle record
            fill_angle(args['angle_record_arr'],
                       count,N_metro-1,R_state,k)
        if record_Js:# assigning last chunk of the J record
            fill_Js(args['J_record_arr'],
                    count,N_metro-1,J_state,k)
            if count >=N_metro:
                print('big count!!! ',count)
        if track_logl:
            logl_tot += count*logl_state
            args['logl_subtot']=logl_tot# recording
        if state_seed:
            args['Rs_state'][k] = R_state 
            args['Js_state'][k] = J_state
