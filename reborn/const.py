import scipy.constants as const

boltzmann_constant = const.value('Boltzmann constant')
classical_electron_radius = const.value('classical electron radius')
speed_of_light = const.c
planck_constant = const.h
avogadros_number = const.N_A
electron_volt = const.value('electron volt')

h = planck_constant
k = boltzmann_constant
c = speed_of_light
hc = const.h*const.c
N_A = avogadros_number
eV = electron_volt