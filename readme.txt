Welcome the the MCEMC public repo.

To run the script make sure you have mpi and the appropriate modules. 
Have some number N of cores accesible. We used 140.
Then run the following command
with the desired number of cores and config filename.
There are four options provided,
a large and small run of constant fluence,
and a large and small run of variable fluence.

    'config_const_J_large.ini'
    'config_const_J_small.ini'
    'config_var_J_large.ini'
    'config_var_J_small.ini'

    mpirun -n N python runner_ll_mcemc.py 'config_name.ini'

The code will create a directory plots/ and a series of folders
that reflect the date and time of running.
The folder labeled the by time will contain
all of the result plots and other write outs.
